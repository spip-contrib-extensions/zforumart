<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// Z
	'zforumart_description' => 'Adaptation du plugin [forumart->https://contrib.spip.net/Squelette-ForuMaRT-V1-PhpBB] d\' aRTHEGONe pour zpip.',
	'zforumart_nom' => 'Zforumart',
	'zforumart_slogan' => 'Forum pour l\'espace public'
);

?>
